﻿[Console]::OutputEncoding = [System.Text.Encoding]::GetEncoding("cp866")
$names = Get-content ".\hnames.txt"
$UserName = "isomov"

Start-Transcript -Append  ".\log.log"

foreach ($name in $names) {
        [Console]::OutputEncoding = [System.Text.Encoding]::GetEncoding("cp866")
        $Query = quser $UserName /server:$name
        
            if($Error.Count -eq 0){
               $ID = ($Query.split('\n')[1]).Split(" ",[System.StringSplitOptions]::RemoveEmptyEntries)[2] 
               Write-Host "reset.exe session $ID /server:$name" -ForegroundColor Red
               reset.exe session $ID /server:$name
                if($Error.Count -gt 0){
                    Write-Host "Сеанс RDP на $name был утерян, но ссесия осталась, ищу ссесию..." -ForegroundColor Red
                    $ID = ($Query.split('\n')[1]).Split(" ",[System.StringSplitOptions]::RemoveEmptyEntries)[1] 
                    Write-Host "reset.exe session $ID /server:$name" -ForegroundColor Red
                    reset.exe session $ID /server:$name
                    $Error.Clear()
                    }
              }
            else{
                if ($Error.TargetObject | findstr "$UserName"){
                
                    $ErrorActionPreference = "silentlycontinue"
                    Write-Host $Error.TargetObject "на сервере" $name -ForegroundColor Green
                    #$Error | Select-Object -Property *
                    $Error.Clear()
                    }
                else{
                
                Write-Host $Error.Exception "$name"  -ForegroundColor Yellow
                $Error.Clear()
                }
                }
        
}
Stop-Transcript